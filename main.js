const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require("body-parser");
const conn = require("./connection");

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/node_modules/bootstrap/dist"));
app.use(bodyParser.urlencoded({ extended: false }));

app.set("view engine", "pug");

//----------producto--------------------
const Product = require("./models/product.model");
app.get("/eliminar/:id", (req, res) => {
  let id = req.params.id;
  Product.findByIdAndRemove(id, (err) => {
    res.redirect("/productos");
  });
});

app.get("/editar/:id", (req, res) => {
  let id = req.params.id;
  Product.findById(id, (err, product) => {
    res.render("detalle", { product });
  });
});
app.get("/crear", (req, res) => {
  res.render("crear");
});
app.post("/completado/:id", (req, res) => {
  let id = req.params.id;
  let fields = {
    nombre: req.body.nombre,
    precio: req.body.precio,
    imagen: req.body.imagen,
  };

  Product.findByIdAndUpdate(req.params.id, { $set: fields }, (err, product) => {
    res.redirect(`/productos`);
  });
});
app.post("/creado", (req, res) => {
  let product = new Product({
    nombre: req.body.nombre,
    precio: req.body.precio,
    imagen: req.body.imagen,
  });
  product.save((err) => (err ? next(err) : res.redirect("/productos")));
});
//--------------------------------------
app.get("/", (req, res) => {
  res.render("home");
});

app.get("/quienes-somos", (req, res) => {
  res.render("about-us");
});

app.get("/contactanos", (req, res) => {
  let data = {};
  let errors = {};
  res.render("contactanos", { errors });
});

app.post("/contactanos", (req, res) => {
  let errors = {};
  let data = req.body;
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var regnumber = /^\d+$/;

  //if (req.body.inputName) {
  //res.render("enviado");
  //} else {
  //errors.name = true;
  //res.render("contactanos", { errors, data });
  //}

  if (!req.body.inputName) errors.name = true;
  if (
    !req.body.inputEmail ||
    !re.test(String(req.body.inputEmail).toLowerCase())
  )
    errors.email = true;
  if (!req.body.inputPhone || !regnumber.test(req.body.inputPhone))
    errors.phone = true;
  if (!req.body.inputMessage) errors.message = true;
  if (!req.body.inputDate) errors.date = true;

  if (
    errors.name ||
    errors.email ||
    errors.phone ||
    errors.message ||
    errors.date
  ) {
    res.render("contactanos", { errors, data });
  } else {
    res.render("enviado");
  }
});

app.get("/productos", (req, res) => {
  Product.find({}).then(function (documents) {
    if (documents) products = documents;
    res.render("productos", { products });
  });
});

app.listen(port, () => {
  console.log(`Escuchando peticiones en el puerto ${port}`);
});
